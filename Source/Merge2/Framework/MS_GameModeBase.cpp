// Fill out your copyright notice in the Description page of Project Settings.


#include "Framework/MS_GameModeBase.h"
#include "MS_GameModeBase.h"
#include "Framework/MS_AIPlayerController.h"
#include "Framework/MS_AIController.h"


void AMS_GameModeBase::StartPlay()
{
	Super::StartPlay();

	SpawnBots();
}

UClass* AMS_GameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController->IsA<AMS_AIPlayerController>()) return AIPlayerPawnClass;

	Super::GetDefaultPawnClassForController_Implementation(InController);

	return nullptr;
}

void AMS_GameModeBase::SpawnBots()
{
	if (!GetWorld() || !AIPlayerControllerClass) return;

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	CurrentController = GetWorld()->SpawnActor<AMS_AIPlayerController>(AIPlayerControllerClass, SpawnInfo);
	RestartPlayer(CurrentController);

}
