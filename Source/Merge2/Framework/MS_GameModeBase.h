// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MS_GameModeBase.generated.h"

class AMS_AIPlayerController;
class AMS_AIController;

UCLASS()
class MERGE2_API AMS_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	virtual void StartPlay() override;

	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "Game")
	TSubclassOf<AMS_AIPlayerController> AIPlayerControllerClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
	TSubclassOf<APawn> AIPlayerPawnClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Controller")
	AMS_AIPlayerController* CurrentController;

	UFUNCTION(BlueprintCallable)
	void SpawnBots();
};
